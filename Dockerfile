# syntax=docker/dockerfile:1
FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC

RUN apt-get update
RUN yes | unminimize
RUN apt-get install -y gcc-multilib g++-multilib clang cmake man-db man \
                       libgmp3-dev libmpfr-dev libx11-6 libx11-dev \
                       texinfo flex bison libmpc-dev libncurses6 libncurses-dev libncursesw6 zlib1g \
                       binutils-arm-none-eabi gcc-arm-none-eabi libnewlib-arm-none-eabi \
                       clang-format nlohmann-json3-dev python3 qtbase5-dev libboost-dev graphviz libyaml-cpp-dev libgtest-dev libgmock-dev libclang-dev
RUN apt-get -y autoremove
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY container-run /usr/local/bin/container-run
RUN chmod +x /usr/local/bin/container-run

ENTRYPOINT [ "/usr/local/bin/container-run" ]
